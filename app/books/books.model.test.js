'use strict'
/* global describe, it, beforeEach, afterEach */

require('should')
const config = require('../config')
const sinon = require('sinon')
const booksModel = require('./books.model')

const sandbox = sinon.createSandbox()

const docClient = {}
booksModel._docClient = docClient

describe('Books API', () => {
  beforeEach(async () => {
    docClient.scan = sandbox.stub().returns({
      promise: sandbox.fake.resolves({})
    })
    docClient.get = sandbox.stub().returns({
      promise: sandbox.fake.resolves({})
    })
    docClient.put = sandbox.stub().returns({
      promise: sandbox.fake.resolves({})
    })
    docClient.update = sandbox.stub().returns({
      promise: sandbox.fake.resolves({})
    })
    docClient.delete = sandbox.stub().returns({
      promise: sandbox.fake.resolves({})
    })
  })

  afterEach(async () => {
    sandbox.restore()
  })

  describe('Get books', () => {
    it('should get all books', async () => {
      await booksModel.getBooks()

      sinon.assert.calledOnce(docClient.scan)
      sinon.assert.calledWithExactly(docClient.scan, {
        TableName: config.aws.table
      })
    })

    it('should get book by uuid', async () => {
      const bookUuid = 'asdfada-aergeare-egaerg'

      await booksModel.getBook(bookUuid)

      sinon.assert.calledOnce(docClient.get)
      sinon.assert.calledWithExactly(docClient.get, {
        TableName: config.aws.table,
        Key: {
          uuid: bookUuid
        }
      })
    })
  })

  describe('Add and update books', () => {
    it('should add new book', async () => {
      const bookUuid = 'asfads-sdfadsf-asdf'
      const bookData = {
        name: 'some name',
        releaseDate: 123213123,
        authorName: 'asdfd'
      }

      await booksModel.addBook(bookUuid, bookData)

      sinon.assert.calledOnce(docClient.put)
      sinon.assert.calledWithExactly(docClient.put, {
        TableName: config.aws.table,
        Item: {
          uuid: bookUuid,
          ...bookData
        }
      })
    })

    it('should update book by uuid', async () => {
      const bookUuid = 'asfads-sdfadsf-asdf'

      const oldBook = {
        uuid: bookUuid,
        name: 'sadf'
      }
      sandbox.stub(booksModel, 'getBook').resolves(oldBook)

      const newBookData = {
        name: 'some name',
        releaseDate: 123213123,
        authorName: 'asdfd'
      }

      await booksModel.updateBook(bookUuid, newBookData)

      sinon.assert.calledOnce(docClient.update)
      sinon.assert.calledWithExactly(docClient.update, {
        TableName: config.aws.table,
        Key: {
          uuid: bookUuid
        },
        ExpressionAttributeNames: {
          '#n': 'name'
        },
        ExpressionAttributeValues: {
          ':n': newBookData.name,
          ':aN': newBookData.authorName,
          ':rD': newBookData.releaseDate
        },
        ReturnValues: 'UPDATED_NEW',
        UpdateExpression: 'set authorName=:aN, #n=:n, releaseDate=:rD'
      })
    })

    it('should not update book if it does not exist', async () => {
      const bookUuid = 'asfads-sdfadsf-asdf'

      const oldBook = undefined
      sandbox.stub(booksModel, 'getBook').resolves(oldBook)

      const newBookData = {
        name: 'some name',
        releaseDate: 123213123,
        authorName: 'asdfd'
      }

      await booksModel.updateBook(bookUuid, newBookData)

      sinon.assert.notCalled(docClient.update)
    })
  })

  describe('Delete books', () => {
    it('should delete book by uuid', async () => {
      const bookUuid = 'asdfada-aergeare-egaerg'

      await booksModel.deleteBook(bookUuid)

      sinon.assert.calledOnce(docClient.delete)
      sinon.assert.calledWithExactly(docClient.delete, {
        TableName: config.aws.table,
        Key: {
          uuid: bookUuid
        }
      })
    })
  })
})
