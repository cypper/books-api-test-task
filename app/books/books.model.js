const AWS = require('aws-sdk')
const config = require('../config')

class BooksModel {
  constructor () {
    AWS.config.update({
      accessKeyId: config.aws.accessKeyId,
      secretAccessKey: config.aws.secretAccessKey,
      region: config.aws.region
    })

    this._docClient = new AWS.DynamoDB.DocumentClient()
    this._table = config.aws.table
  }

  async getBooks () {
    const params = {
      TableName: this._table
    }

    const {
      Items: books
    } = await this._dbRequest('scan', params)

    return books
  }

  async getBook (uuid) {
    const params = {
      TableName: this._table,
      Key: {
        uuid: uuid
      }
    }

    const {
      Item: book
    } = await this._dbRequest('get', params)

    return book
  }

  async addBook (uuid, bookData) {
    const {
      name,
      releaseDate,
      authorName
    } = bookData

    const params = {
      TableName: this._table,
      Item: {
        uuid,
        name,
        releaseDate,
        authorName
      }
    }

    await this._dbRequest('put', params)
  }

  async updateBook (uuid, bookData) {
    const {
      name,
      releaseDate,
      authorName
    } = bookData

    const book = await this.getBook(uuid)
    if (!book) return

    const params = {
      TableName: this._table,
      Key: {
        uuid
      },
      UpdateExpression: 'set authorName=:aN, #n=:n, releaseDate=:rD',
      ExpressionAttributeNames: {
        '#n': 'name'
      },
      ExpressionAttributeValues: {
        ':aN': authorName,
        ':rD': releaseDate,
        ':n': name
      },
      ReturnValues: 'UPDATED_NEW'
    }

    await this._dbRequest('update', params)
  }

  async deleteBook (uuid) {
    const params = {
      TableName: this._table,
      Key: {
        uuid
      }
    }

    await this._dbRequest('delete', params)
  }

  _dbRequest (type, params) {
    return this._docClient[type](params).promise()
  }
}

module.exports = new BooksModel()
