const booksModel = require('./books.model')
const uuid = require('uuid/v4')
const logger = require('log4js').getLogger('BooksService')
logger.level = 'debug'

class BooksService {
  get getBooks () {
    return this._getBooks.bind(this)
  }

  get getBook () {
    return this._getBook.bind(this)
  }

  get addBook () {
    return this._addBook.bind(this)
  }

  get updateBook () {
    return this._updateBook.bind(this)
  }

  get deleteBook () {
    return this._deleteBook.bind(this)
  }

  async _getBooks (ctx) {
    ctx.body = await booksModel.getBooks()
  }

  async _getBook (ctx) {
    const bookUuid = ctx.params.bookUuid
    const book = await booksModel.getBook(bookUuid)

    if (book) {
      logger.info(`Book ${bookUuid} is found`)
      ctx.body = book
    } else {
      logger.info(`Book ${bookUuid} is not found`)
      ctx.status = 404
    }
  }

  async _addBook (ctx) {
    try {
      const bookData = JSON.parse(ctx.req.body.toString())
      this._validateBookData(bookData)

      const bookUuid = uuid()
      await booksModel.addBook(bookUuid, bookData)

      logger.info(`Book ${bookUuid} has been added`)
      ctx.status = 204
    } catch (e) {
      logger.error('Book has not been added because of an error', e)
      ctx.status = 400
    }
  }

  async _updateBook (ctx) {
    const bookUuid = ctx.params.bookUuid

    try {
      const bookData = JSON.parse(ctx.req.body.toString())
      this._validateBookData(bookData)

      await booksModel.updateBook(bookUuid, bookData)

      logger.info(`Book ${bookUuid} has been updated`)
      ctx.status = 204
    } catch (e) {
      logger.error(`Book ${bookUuid} has not been updated because of an error`, e)
      ctx.status = 400
    }
  }

  async _deleteBook (ctx) {
    const bookUuid = ctx.params.bookUuid
    await booksModel.deleteBook(bookUuid)

    logger.info(`Book ${bookUuid} has been deleted`)
    ctx.status = 204
  }

  _validateBookData (data) {
    if (typeof data !== 'object') throw new Error('Book data should be an object')
    if (typeof data.name !== 'string') throw new Error('Book name should be a string')
    if (typeof data.releaseDate !== 'number') throw new Error('Book release date should be a number')
    if (typeof data.authorName !== 'string') throw new Error('Book author name should be a string')
  }
}

module.exports = new BooksService()
