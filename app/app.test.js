'use strict'
/* global describe, it, afterEach */

require('should')
const lambda = require('./app')
const booksModel = require('./books/books.model')
const lambdaTester = require('lambda-tester')
const sinon = require('sinon')

const sandbox = sinon.createSandbox()

describe('Books API router', () => {
  afterEach(async () => {
    sandbox.restore()
  })

  describe('Get books', () => {
    it('should get books', async () => {
      const books = [{
        book1: '1'
      }, {
        book2: '2'
      }]
      sandbox.stub(booksModel, 'getBooks').resolves(books)

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'GET',
          path: '/books'
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(200)
          result.headers['content-type'].should.startWith('application/json')
          result.headers['content-length'].should.be.eql('29')
          JSON.parse(result.body).should.deepEqual(books)
        })

      sinon.assert.calledOnce(booksModel.getBooks)
      sinon.assert.calledWithExactly(booksModel.getBooks)
    })

    it('should get book by uuid', async () => {
      const bookUuid = 'asldkfasd-asdgsdag-asgsfe'
      const book = {
        book1: '1'
      }
      sandbox.stub(booksModel, 'getBook').resolves(book)

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'GET',
          path: `/book/${bookUuid}`
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(200)
          result.headers['content-type'].should.startWith('application/json')
          result.headers['content-length'].should.be.eql('13')
          JSON.parse(result.body).should.deepEqual(book)
        })

      sinon.assert.calledOnce(booksModel.getBook)
      sinon.assert.calledWithExactly(booksModel.getBook, bookUuid)
    })

    it('should return 404 if book does not exist', async () => {
      const bookUuid = 'asldkfasd-asdgsdag-asgsfe'
      sandbox.stub(booksModel, 'getBook').resolves()

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'GET',
          path: `/book/${bookUuid}`
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(404)
          result.body.should.eql('Not Found')
        })

      sinon.assert.calledOnce(booksModel.getBook)
      sinon.assert.calledWithExactly(booksModel.getBook, bookUuid)
    })
  })

  describe('Add books', () => {
    it('should add new book', async () => {
      const bookData = { name: 'some name', releaseDate: 1242142133, authorName: 'author' }
      sandbox.stub(booksModel, 'addBook').resolves()

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'POST',
          path: '/book/add',
          body: bookData
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(204)
          result.body.should.eql('')
        })

      sinon.assert.calledOnce(booksModel.addBook)
      sinon.assert.calledWithExactly(booksModel.addBook, sinon.match(''), bookData)
    })

    it('should validate new book data', async () => {
      sandbox.stub(booksModel, 'addBook').resolves()

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'POST',
          path: '/book/add',
          body: {}
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(400)
          result.body.should.eql('Bad Request')
        })

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'POST',
          path: '/book/add',
          body: { name: 21321, releaseDate: 1242142133, authorName: 'author' }
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(400)
          result.body.should.eql('Bad Request')
        })

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'POST',
          path: '/book/add',
          body: { name: 'some name', releaseDate: 1242142133 }
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(400)
          result.body.should.eql('Bad Request')
        })

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'POST',
          path: '/book/add',
          body: { name: 'some name', authorName: 'author' }
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(400)
          result.body.should.eql('Bad Request')
        })

      sinon.assert.notCalled(booksModel.addBook)
    })
  })

  describe('Update books', () => {
    it('should update book', async () => {
      const bookUuid = 'asdlk-aeaerg-aergearg-adg'
      const bookData = { name: 'some name', releaseDate: 1242142133, authorName: 'author' }
      sandbox.stub(booksModel, 'updateBook').resolves()

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'POST',
          path: `/book/${bookUuid}/update`,
          body: bookData
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(204)
          result.body.should.eql('')
        })

      sinon.assert.calledOnce(booksModel.updateBook)
      sinon.assert.calledWithExactly(booksModel.updateBook, bookUuid, bookData)
    })

    it('should validate update book data', async () => {
      const bookUuid = 'asdlk-aeaerg-aergearg-adg'
      sandbox.stub(booksModel, 'updateBook').resolves()

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'POST',
          path: `/book/${bookUuid}/update`,
          body: {}
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(400)
          result.body.should.eql('Bad Request')
        })

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'POST',
          path: `/book/${bookUuid}/update`,
          body: { name: 21321, releaseDate: 1242142133, authorName: 'author' }
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(400)
          result.body.should.eql('Bad Request')
        })

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'POST',
          path: `/book/${bookUuid}/update`,
          body: { name: 'some name', releaseDate: 1242142133 }
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(400)
          result.body.should.eql('Bad Request')
        })

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'POST',
          path: `/book/${bookUuid}/update`,
          body: { name: 'some name', authorName: 'author' }
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(400)
          result.body.should.eql('Bad Request')
        })

      sinon.assert.notCalled(booksModel.updateBook)
    })
  })

  describe('Delete books', () => {
    it('should delete book', async () => {
      const bookUuid = 'asdlk-aeaerg-aergearg-adg'
      sandbox.stub(booksModel, 'deleteBook').resolves()

      await lambdaTester(lambda.booksAPI)
        .event({
          httpMethod: 'POST',
          path: `/book/${bookUuid}/delete`
        })
        .expectResult(result => {
          result.statusCode.should.be.eql(204)
          result.body.should.eql('')
        })

      sinon.assert.calledOnce(booksModel.deleteBook)
      sinon.assert.calledWithExactly(booksModel.deleteBook, bookUuid)
    })
  })
})
