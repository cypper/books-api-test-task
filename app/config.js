const config = {
  aws: {
    region: 'us-west-2',
    accessKeyId: process.env.DB_AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.DB_AWS_SECRET_ACCESS_KEY,
    table: 'Books'
  }
}

module.exports = config
