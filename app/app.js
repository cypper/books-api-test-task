'use strict'

const serverless = require('serverless-http')
const Koa = require('koa')
const router = require('./app.router')

const app = new Koa()

app.use(router.routes())

module.exports.booksAPI = serverless(app)
