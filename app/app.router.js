const Router = require('koa-router')
const router = new Router()
const booksService = require('./books/books.service')

router.get('/books', booksService.getBooks)
router.get('/book/:bookUuid', booksService.getBook)
router.post('/book/add', booksService.addBook)
router.post('/book/:bookUuid/update', booksService.updateBook)
router.post('/book/:bookUuid/delete', booksService.deleteBook)

module.exports = router
